package com.musicServer.fileManager;

import java.util.concurrent.ConcurrentHashMap;


public class TreeNode<T> {

    private String key;//not null
    private T path;// not null
    private TreeNode<T> parent;
    private ConcurrentHashMap<String, TreeNode<T>> children;
    private boolean isRootNode;
    private boolean checked = false;

    public TreeNode() {
        this.path = null;
        this.children = null;
        this.parent = null;
        isRootNode = true;
        this.key = null;
    }

    public TreeNode(String key, T path) { //empty root node, no parent
        this.path = path;
        this.children = null;
        this.parent = null;
        isRootNode = true;
        this.key = key;
    }

    public TreeNode<T> addChild(String key, T child) {
        if (children == null)
            this.children = new ConcurrentHashMap<String, TreeNode<T>>();
        TreeNode<T> childNode = new TreeNode<T>(key, child);
        childNode.setParent(this);
        childNode.isRootNode = false;
        return this.children.computeIfAbsent(key, c -> childNode);
    }

    public TreeNode<T> addChild(String key, TreeNode<T> childNode) {
        if (children == null)
            this.children = new ConcurrentHashMap<String, TreeNode<T>>();
        childNode.setParent(this);
        childNode.isRootNode = false;
        childNode.setKey(key);
        return this.children.computeIfAbsent(key, c -> childNode);

    }

    public void removeChild(String key) {
        if (contains(key))
            children.remove(key);
    }

    public boolean contains(String key) {
        if (children != null)
            return this.children.containsKey(key);
        else return false;
    }


    public T getPath() {
        return path;
    }

    public TreeNode getParent() {
        return parent;
    }

    ConcurrentHashMap<String, TreeNode<T>> getChildren() {
        return children;
    }

    public TreeNode<T> getChild(String key) {
        if (children != null)
            return children.getOrDefault(key, null);
        else return null;
    }

    public boolean isRootNode() {
        return isRootNode;
    }

    private void setPath(T path) {
        this.path = path;
    }

    private void setParent(TreeNode<T> parent) {
        isRootNode = false;
        this.parent = parent;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
        if (children != null)
            children.forEach((x, y) -> y.setChecked(checked));
    }

    @Override
    public String toString() {
        return key;
    }


    public boolean equals(TreeNode<T> node) {
        //if (this == node) return true;
        if (this.key==node.getKey() || this.key.equals(node.getKey())) {
            if (this.path == node.getPath()|| this.path.equals(node.getPath())) {
                if (this.parent == node.getParent() || this.parent.equals(node.getParent())) {
                    if (this.children == node.getChildren() || this.children.equals(node.getChildren())) {
                        if (this.isRootNode == node.isRootNode) {
                            if (this.checked == node.checked) {
                                return true;
                            }
                        } else return false;
                    }else return false;
                }else return false;
            }else return false;
        }else return false;
        return false;
    }


}

