package com.musicServer.server.models;


import org.springframework.stereotype.Component;

@Component
public class RequestFile {

    private String file;

    public RequestFile(){};

    public RequestFile (String file){
        this.file = file;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }
}
