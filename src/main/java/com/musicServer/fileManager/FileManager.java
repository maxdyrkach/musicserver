package com.musicServer.fileManager;

import javafx.scene.control.CheckBoxTreeItem;


import java.nio.file.Path;
import java.util.concurrent.ConcurrentHashMap;

//методы удаления, добавления, обновлениия, очистки дерева

public class FileManager {

    private ConcurrentHashMap<String,FileTree> fileTreeMap = new ConcurrentHashMap<>();
    private CheckBoxTreeItem<TreeNode<Path>> rootTreeItem = new CheckBoxTreeItem<>(new TreeNode<>());

    public ConcurrentHashMap<String,FileTree> getFileTreeMap() {
        return fileTreeMap;
    }

    public FileTree build(String initial) {
        FileTree fileTree = FileTreeBuilder.build(initial);
        fileTreeMap.computeIfAbsent(fileTree.getRoot().getPath().toAbsolutePath().toString().
                replace("\\","/"),x->fileTree);
        addCheckBoxTree(rootTreeItem, fileTree.getRoot());
        return fileTree;
    }

    public CheckBoxTreeItem<TreeNode<Path>> getRootTreeItem() {
       return rootTreeItem;
    }

    public CheckBoxTreeItem<TreeNode<Path>> addCheckBoxTree(CheckBoxTreeItem<TreeNode<Path>> rootTreeItem, TreeNode<Path> node) {
        CheckBoxTreeItem<TreeNode<Path>> childTreeItem = new CheckBoxTreeItem<>(node);
        rootTreeItem.getChildren().add(childTreeItem);
        if (node.getChildren() != null) {
            node.getChildren().values().stream().sorted((a, b) -> {
                return a.getKey().compareTo(b.getKey());
            }).forEach((x) -> {
                addCheckBoxTree(childTreeItem, x);
            });
        }
        return childTreeItem;
    }

    FileTree cropFileTree(FileTree fileTree) {
        class Cropper {
            TreeNode<Path> checkedOnly(TreeNode<Path> node) {
                if (node.isChecked() && node.getChildren() != null) {
                    node.getChildren().forEach((k, v) -> {
                        if (!v.isChecked())
                            node.removeChild(k);
                    });
                    return node;
                }
                return null;
            }
        }
        Cropper cropper = new Cropper();
        TreeNode<Path> node = fileTree.getRoot();
        cropper.checkedOnly(node);
        node.getChildren().forEach((k, v) -> {
            cropper.checkedOnly(v);
        });

        return fileTree;
    }

    public void renew(){
        fileTreeMap.values().stream().forEach(x->x.renew());

    }

   // public CheckBoxTreeItem<TreeNode<Path>> pasteNode();


}
