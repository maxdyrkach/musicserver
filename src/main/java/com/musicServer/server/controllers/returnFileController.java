package com.musicServer.server.controllers;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLConnection;


@RestController
public class returnFileController {

    @RequestMapping(value = "/getmp3", produces = {"multipart/form-data"}, method = RequestMethod.GET)
    public @ResponseBody ResponseEntity getFile (HttpServletRequest request, HttpServletResponse response,
                            @RequestParam (value = "addr", required = true) String address) {


        ResponseEntity responseEntity = null;
        byte[] reportBytes = null;

        File returnFile = new File(address);
       if (returnFile.exists()){
           try {
               InputStream inputStream = new FileInputStream(returnFile);
               ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
               int bytesRead;


               String type = URLConnection.guessContentTypeFromName(returnFile.getName());
               byte [] out2 = new byte [4096];

               while ((bytesRead = inputStream.read(out2,0,out2.length)) !=-1){
                   byteArrayOutputStream.write(out2,0,bytesRead);
               }

               //new FileInputStream(returnFile).getChannel().read(ByteBuffer.wrap(out2));

               //byte[]out = org.apache.commons.io.IOUtils.toByteArray(bis);

               HttpHeaders responseHeaders = new HttpHeaders();
               responseHeaders.add("content-disposition", "attachment; filename=" + returnFile.getName());
               responseHeaders.add("Content-Type",type);

               responseEntity = new ResponseEntity(byteArrayOutputStream.toByteArray(), responseHeaders, HttpStatus.OK);
           } catch (IOException e) {
               e.printStackTrace();
           }

       } else {
           responseEntity = new ResponseEntity ("File Not Found", HttpStatus.OK);
       }
       return responseEntity;

    }
}
