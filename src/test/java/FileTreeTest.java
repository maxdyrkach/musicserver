import com.musicServer.fileManager.FileTree;

import static org.junit.Assert.*;

import com.musicServer.fileManager.FileTrees;
import com.musicServer.fileManager.TreeNode;
import org.junit.Before;
import org.junit.Test;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileTreeTest {
    private FileTree fileTree;
    TreeNode<Path> child1;
    TreeNode<Path> child2;
    TreeNode<Path> child3;
    TreeNode<Path> child3_1;
    TreeNode<Path> child3_1_1;
    TreeNode<Path> child3_1_2;


    @Before
    public void initTest() {
        fileTree = new FileTree(Paths.get("e:/My Downloads"));
        child1 = fileTree.addChild(fileTree.getRoot(), Paths.get("E:/My Downloads/Stone Sour - Hydrograd (2017)"));
        child2 = fileTree.addChild(fileTree.getRoot(), Paths.get("E:/My Downloads/The Charlatans - Different Days [2017]"));
        child3 = fileTree.addChild(fileTree.getRoot(), Paths.get("E:/My Downloads/I See Stars ALAC"));
        child3_1 = fileTree.addChild(child3, Paths.get("E:/My Downloads/I See Stars ALAC/2009 - 3D"));
        child3_1_1 = fileTree.addChild(child3_1,
                Paths.get("E:/My Downloads/I See Stars ALAC/2009 - 3D/09 Sing This!.m4a"));
        child3_1_2 = fileTree.addChild(child3_1,
                Paths.get("E:/My Downloads/I See Stars ALAC/2009 - 3D/111.txt"));
        System.out.println(fileTree.getRoot().getPath().relativize(child3_1_1.getPath()).toString());
        System.out.println(child3_1_1.getPath().relativize(fileTree.getRoot().getPath()).toString());
        System.out.println(child1.getPath().relativize(child2.getPath()).toString());
        System.out.println(fileTree.getRoot().getPath().relativize(fileTree.getRoot().getPath()).toString());

    }

    @Test
    public void findTest() {
        assertEquals(child1, FileTrees.find(fileTree, Paths.get("E:/My Downloads/Stone Sour - Hydrograd (2017)")));
        assertTrue(child3_1_1.equals(FileTrees.find(fileTree.getRoot(), child3_1_1.getPath())));

        assertTrue(child3_1_1.equals(FileTrees.find(fileTree.getRoot(), child3_1_1.getPath())));


        FileTrees.renew(fileTree);
        FileTrees.print(fileTree);

    }

}
