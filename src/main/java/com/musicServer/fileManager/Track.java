package com.musicServer.fileManager;

import com.musicServer.gui.FXMain;
import javafx.collections.MapChangeListener;
import javafx.scene.image.ImageView;
import javafx.scene.media.Media;

import java.io.File;
import java.nio.file.Path;
import java.util.Date;

public class Track {

    private Path filePath;
    private String filename; //without extension
    private String artist;
    private String album;
    private String title;
    private String year;
    private int numberInAlbum;
    private Path albumCover;
    private double duration;

    public Track() {
    }

    public Track(Path file) {
        if (file.toFile().exists()) {
            if (file.getFileName().toString().toLowerCase().endsWith(".mp3")) {
                filePath = file;
                String str = file.getFileName().toString();
                filename = str.substring(str.length() - 1 - 4);
                Media media = new Media(file.toUri().toString());
                duration = media.getDuration().toSeconds();
                media.getMetadata().addListener((MapChangeListener.Change <? extends String, ? extends Object> ch)->{
                    if (ch.wasAdded()){
                        String key = ch.getKey();
                        switch (key){
                            case "artist":
                                this.artist = (String)ch.getValueAdded();
                                break;
                            case "year":
                                this.year = (String) ch.getValueAdded();
                                break;
                            case "album":
                                this.album = (String) ch.getValueAdded();
                                break;
                            case"title":
                                this.title = (String) ch.getValueAdded();
                                break;
                            case "№":
                            case"#":
                            case"number":
                                this.numberInAlbum = Integer.parseInt((String)ch.getValueAdded());
                                break;
                        }

                    }
                });

            }else System.out.println("Wrong filename");
        }else System.out.println("File does not exist");
    }

    public Path getFilePath() {
        return filePath;
    }

    public String getFilename() {
        return filename;
    }

    public double getDuration() {
        return duration;
    }

    public void setFilePath(Path filePath) {
        if (filePath.toFile().exists()) {
            if (filePath.getFileName().toString().toLowerCase().endsWith(".mp3")) {
                this.filePath = filePath;
                String str = filePath.getFileName().toString();
                filename = str.substring(str.length() - 1 - 4);
                Media media = new Media(filePath.toUri().toString());
                duration = media.getDuration().toSeconds();
               /* media.getMetadata().forEach((name, val) -> {
                    System.out.println(name + " = " + val);
                });*/

            }else System.out.println("Wrong filename");
        }else System.out.println("File does not exist");

    }

    public static void main(String[] args) {
        FXMain.main(null);
        Path path = new File("e:/04.MP3").toPath();
        //Track track = new Track(path);
        Media media = new Media(path.toUri().toString());
        media.getMetadata().forEach((name, val) -> {
            System.out.println(name + " = " + val);
        });

    }


}
