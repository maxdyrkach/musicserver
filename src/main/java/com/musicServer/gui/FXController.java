package com.musicServer.gui;

import com.musicServer.fileManager.FileManager;
import com.musicServer.fileManager.FileTrees;
import com.musicServer.fileManager.TreeNode;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableMap;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTreeCell;
import javafx.stage.DirectoryChooser;
import javafx.stage.Window;
import javafx.util.Callback;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


public class FXController {

    @FXML
    private TreeTableView treeTable;
    @FXML
    private TreeTableColumn checkColumn, fileNameColumn;
    @FXML
    private Button chooseBtn, renewBtn, addBtn, delBtn;
    @FXML
    private TreeView treeView;
    @FXML
    private TextField choosingTxfield;

    private File initDir = new File(System.getProperty("user.home"));
    private FileManager fileManager;
    private File chosen;

    private Window window;


    public void onChoosePressed(ActionEvent event) {
        DirectoryChooser chooser = new DirectoryChooser();
        chooser.setTitle("Выбрать корневой каталог:");
        chooser.setInitialDirectory(initDir);
        chosen = chooser.showDialog(((Button) event.getSource()).getScene().getWindow());
        if (chosen != null) {
            choosingTxfield.setText(chosen.getAbsolutePath());
        }
    }

    public void onRenewBtnPressed(ActionEvent event) {
        //rebuildTreeView();
        renewTreeView();
    }

    public void renewTreeView() {
        fileManager.renew();
        treeView.refresh();
    }

    public void printBranch(ActionEvent event) {
        fileManager.print();
    }

    public void rebuildTreeView() {
        CheckBoxTreeItem<ObservableMap.Entry<String,TreeNode<Path>>> item = new CheckBoxTreeItem<>("", new TreeNode<>());
        fileManager.setRootTreeItem(item);
        fileManager.getFileTreeMap().values().stream().forEach((x) -> {
            FileTrees.renew(x);
            TreeNode<Path> node = x.getRoot();
            fileManager.addCheckBoxTree(item, node);
        });
        treeView.setRoot(item);
        item.setExpanded(true);
        treeView.setShowRoot(false);

    }


    public void onAddBtnPressed(ActionEvent event) {
        if (choosingTxfield.getText().isEmpty() || !Files.exists(Paths.get(choosingTxfield.getText()))) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION,
                    "You must choose directory before adding!", ButtonType.OK);
            alert.initOwner(window);
            alert.show();
        }
        else{
            Path path = Paths.get(choosingTxfield.getText());
            TreeNode<Path> findNode = fileManager.getFileTreeMap().values().stream().map((x)->{
                return FileTrees.find(x,path);
            }).filter((x)-> x!=null).findFirst().get();
            if(findNode==null)
                return;
            fileManager.build(choosingTxfield.getText());
            fileManager.getRootTreeItem().setExpanded(true);
            treeView.setRoot(fileManager.getRootTreeItem());
            treeView.setShowRoot(false);
            treeView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

            treeView.setCellFactory(new Callback<TreeView<TreeNode<Path>>, CheckBoxTreeCell<TreeNode<Path>>>() {
                @Override
                public CheckBoxTreeCell<TreeNode<Path>> call(TreeView<TreeNode<Path>> param) {
                    CheckBoxTreeCell<TreeNode<Path>> treeCell = new CheckBoxTreeCell
                            (new Callback<CheckBoxTreeItem<TreeNode<Path>>, ObservableValue<Boolean>>() {

                                @Override
                                public ObservableValue<Boolean> call(CheckBoxTreeItem<TreeNode<Path>> param1) {
                                    TreeNode<Path> node = param1.getValue();

                                    // property.bindBidirectional(node.checkedProperty());
                    /*property.addListener(new ChangeListener<Boolean>() {
                        @Override
                        public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                            node.setChecked(newValue);
                        }
                    });*/

                                    return node.checkedProperty();
                                }
                            });


                    return treeCell;
                }
            });


        }


    }

    public void onDelBtnPressed(ActionEvent event) {

    }

    public void onClearBtnPressed(ActionEvent event) {

    }


    public TreeTableView getTreeTable() {
        return treeTable;
    }

    public TreeTableColumn getCheckColumn() {
        return checkColumn;
    }

    public TreeTableColumn getFileNameColumn() {
        return fileNameColumn;
    }

    public TreeView getTreeView() {
        return treeView;
    }

    public void setFileManager(FileManager fileManager) {
        this.fileManager = fileManager;
    }

    public Window getWindow() {
        return window;
    }

    public void setWindow(Window window) {
        this.window = window;
    }
}
