package com.musicServer.fileManager;

import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;

public class FileTree implements Serializable {

    private TreeNode<Path> root;
    private Path rootPoint;


    public FileTree(Path initial) {
        root = new TreeNode<Path>(initial.getFileName().toString(), initial.toAbsolutePath());
        rootPoint = initial.getRoot().toAbsolutePath();
        root.setKey(initial.getFileName().toString());
    }

    public TreeNode<Path> addChild(TreeNode<Path> node, Path path) {
        return node.addChild(path.getFileName().toString(), path.toAbsolutePath());
    }

    public TreeNode<Path> addChild(TreeNode<Path> node, TreeNode<Path> childNode) {
        return node.addChild(childNode.getPath().getFileName().toString(), childNode);
    }

    public void removeChild(TreeNode<Path> node, Path path) {
        String key = path.getFileName().toString();
        if (node.contains(key))
            node.removeChild(key);
    }

    public boolean contains(TreeNode<Path> node, Path path) {
        return node.contains(path.getFileName().toString());
    }

    public Path getChild(TreeNode<Path> node, String fileName) {
        if (node.contains(fileName))
            return node.getChildren().get(fileName).getPath();
        else return null;
    }

    public List<Path> getChildren(TreeNode<Path> node) {
        return node.getChildren().values().stream().map(TreeNode::getPath).collect(Collectors.toList());
    }

    public TreeNode<Path> getRoot() {
        return root;
    }



    public TreeNode<Path> find(TreeNode<Path> node, Path path) {
        UnaryOperator<TreeNode<Path>> unary = node12 -> {
            try {
                System.out.println("Processing: " + node12.getPath().toString() +
                        " equals " + Files.isSameFile(node12.getPath().toAbsolutePath(), path.toAbsolutePath()) +" "+
                        path.toAbsolutePath().toString());
                if (Files.isSameFile(node12.getPath().toAbsolutePath(), path.toAbsolutePath())) {
                    System.out.println("Abs: " + node12.getPath().toAbsolutePath()+
                              ", toString: "+ node12.toString());
                    return node12;
                } else
                    return null;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        };

        return unaryOnNodeTree(node, unary);
    }

    public void print() {
        printBranch(root, 0);
    }

    public void printBranch(TreeNode<Path> node, int level) {
        for (int i = 0; i < level; i++)
            System.out.print(" ");
        System.out.println(node.getPath().getFileName());
        if (node.getChildren() != null) {
            level++;
            int finalLevel = level;
            node.getChildren().values().stream().forEach((x) -> {
                printBranch(x, finalLevel);
            });
        }
    }

    public TreeNode<Path> pastePath(Path path) {
        try {
            if (Files.isSameFile(path.getRoot().toAbsolutePath(), rootPoint)) {
                path.getRoot().toAbsolutePath().forEach(null);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void renew() {
        Consumer<TreeNode<Path>> consumer = new Consumer<TreeNode<Path>>() {
            @Override
            public void accept(TreeNode<Path> node) {
                if (!Files.exists(node.getPath()))
                    node.getParent().removeChild(node.getKey());
            }
        };
        root = consumerOnNodeTree(root, consumer);
    }

    public TreeNode<Path> consumerOnNodeTree(TreeNode<Path> rootNode, Consumer<TreeNode<Path>> consumer) {
        consumer.accept(rootNode);
        if (rootNode.getChildren() != null) {
            rootNode.getChildren().forEach((k, v) -> consumerOnNodeTree(v, consumer));
        }
        return rootNode;
    }

    public boolean predicateOnNodeTree(TreeNode<Path> rootNode, Predicate<TreeNode<Path>> predicate) {
        AtomicBoolean result = new AtomicBoolean(false);
        result.set(predicate.test(rootNode));
        if (result.get()) return result.get();
        if (rootNode.getChildren() != null) {
            result.set(rootNode.getChildren().values().stream().anyMatch((v) -> (predicateOnNodeTree(v, predicate))));
        }
        return result.get();
    }

    public TreeNode<Path> unaryOnNodeTree(TreeNode<Path> rootNode, UnaryOperator<TreeNode<Path>> unary) {
        TreeNode<Path> result = null;
        result = unary.apply(rootNode);
        if (result != null) return result;
        if (rootNode.getChildren() != null) {
            result = rootNode.getChildren().values().stream()
                    .map((v) -> (unaryOnNodeTree(v, unary))).filter((x)->(x!=null)).findFirst().orElse(null);
            return result;
        }
        return result;
    }

}
