package com.musicServer.gui;

import com.musicServer.fileManager.FileManager;
import com.musicServer.fileManager.FileTree;
import com.musicServer.fileManager.TreeNode;
import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTreeCell;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.nio.file.Path;

public class FXMain extends Application {

    FileManager fileManager;
    TreeNode<Path> node;
    FXController controller;

    @Override
    public void start(Stage primaryStage) throws Exception {

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/gui/mainStage.fxml"));
        Parent root = fxmlLoader.load();
        controller = fxmlLoader.getController();//controller
        primaryStage.setTitle("Music Server");
        primaryStage.setResizable(true);
        primaryStage.setScene(new Scene(root, 600, 480));
        fileManager = new FileManager();
        controller.setFileManager(fileManager);
       // initTree();
        controller.setWindow(primaryStage.getOwner());
        primaryStage.show();

    }

    private void initTree (){
        //fileManager.build("E:/My Downloads");
        //FileTree tree = fileManager.getFileTreeMap().get("E:/My Downloads");
        //node = tree.getRoot();
        //CheckBoxTreeItem<TreeNode<Path>> item = new CheckBoxTreeItem<>(new TreeNode<>());
        //item.setExpanded(true);

        //fileManager.addCheckBoxTree(item, node);
        TreeView treeView = controller.getTreeView();
        //treeView.setRoot(item);
        //treeView.setShowRoot(false);


        treeView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        treeView.setCellFactory(new Callback<TreeView<TreeNode<Path>>, CheckBoxTreeCell<TreeNode<Path>>>() {
            @Override
            public CheckBoxTreeCell<TreeNode<Path>> call(TreeView<TreeNode<Path>> param) {
                CheckBoxTreeCell<TreeNode<Path>> treeCell = new CheckBoxTreeCell
                        (new Callback<CheckBoxTreeItem<TreeNode<Path>>, ObservableValue<Boolean>>() {

                    @Override
                    public ObservableValue<Boolean> call(CheckBoxTreeItem<TreeNode<Path>> param1) {
                        TreeNode<Path> node = param1.getValue();

                        // property.bindBidirectional(node.checkedProperty());
                    /*property.addListener(new ChangeListener<Boolean>() {
                        @Override
                        public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                            node.setChecked(newValue);
                        }
                    });*/

                        return node.checkedProperty();
                    }
                });


                return treeCell;
            }
        });




    }

    @Override
    public void stop() throws Exception {
        com.musicServer.server.Application.getContext().close();
        super.stop();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
