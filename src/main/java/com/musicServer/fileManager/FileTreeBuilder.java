package com.musicServer.fileManager;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

public class FileTreeBuilder implements FileVisitor<Path> {

    private FileTree fileTree;
    private String fileName;
    private Path initial;
    private TreeNode<Path> currentNode;
    private boolean loggingEnabled = false;
    private boolean emptyDir = true;


    private FileTreeBuilder(Path initial) {
        this.initial = initial;
        this.fileTree = new FileTree(initial);
        currentNode = fileTree.getRoot();
    }

    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
        emptyDir = true;
        if (loggingEnabled)
            System.out.println("New dir: " + dir);
        if (fileTree == null)
            fileTree = new FileTree(initial);
        if (Files.isSameFile(dir, initial)) {
            currentNode = fileTree.getRoot();
            return FileVisitResult.CONTINUE;
        } else if (Files.isSameFile(dir.toAbsolutePath().getParent(), currentNode.getPath().toAbsolutePath()) && !fileTree.contains(currentNode, dir)) {
            currentNode = fileTree.addChild(currentNode, dir);
            if (loggingEnabled)
                System.out.println("Entered to: " + dir.toString());
            return FileVisitResult.CONTINUE;
        } else {
            if (loggingEnabled)
                System.out.println("Some mistake? wrong dir");
            return FileVisitResult.TERMINATE;
        }
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        if (file.getFileName().toString().toLowerCase().endsWith(".mp3")) {
            fileTree.addChild(currentNode, file);
            emptyDir = false;
            if (loggingEnabled)
                System.out.println("Added file " + file.getFileName() + " to " + currentNode.getPath().getFileName());
        }
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
        if (loggingEnabled)
            System.out.println("Access failed to " + file.toString() + " \n" + exc.getMessage());
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {

        currentNode = currentNode.getParent();
        if(emptyDir && currentNode!=null)
            currentNode.removeChild(dir.getFileName().toString());
        if (loggingEnabled)
            System.out.println("Finished visiting: " + dir.getFileName());
        return FileVisitResult.CONTINUE;

    }

    public static FileTree build(String initial) {
        Path initP = Paths.get(initial);
        if (initP != null) {
            FileTreeBuilder builder = new FileTreeBuilder(initP);
            try {
                Files.walkFileTree(initP, builder);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return builder.getFileTree();
        }
        return null;
    }

    public FileTree getFileTree() {
        return this.fileTree;
    }

    public static void main(String[] args) {
        FileTreeBuilder builder = new FileTreeBuilder(Paths.get("E:/My Downloads"));
        try {
            Files.walkFileTree(Paths.get("E:/My Downloads"), builder);
        } catch (IOException e) {
            e.printStackTrace();
        }
        builder.fileTree.print();
    }
}
