package com.musicServer.server.models;


import org.springframework.stereotype.Component;

import java.io.File;

@Component
public class HttpReturnFileEntity {

    private File file;

    public HttpReturnFileEntity(){}

    public HttpReturnFileEntity(File file){
        this.file=file;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
}
