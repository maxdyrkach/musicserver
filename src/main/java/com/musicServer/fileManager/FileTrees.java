package com.musicServer.fileManager;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

public class FileTrees {

    public static TreeNode<Path> find(FileTree fileTree, Path path) {
        return find(fileTree.getRoot(), path);
    }

    public static TreeNode<Path> find(TreeNode<Path> node, Path path) {
        UnaryOperator<TreeNode<Path>> unary = node12 -> {
            try {
                if (Files.isSameFile(node12.getPath().toAbsolutePath(), path.toAbsolutePath())) {
                    return node12;
                } else
                    return null;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        };

        return unaryOnNodeTree(node, unary);
    }

    public static void print(FileTree fileTree) {
        printBranch(fileTree.getRoot(), 0);
    }

    public static void printBranch(TreeNode<Path> node, int level) {
        for (int i = 0; i < level; i++)
            System.out.print(" |");
        System.out.print("-");
        System.out.println(node.getPath().getFileName() + " " + node.isChecked());
        if (!node.getChildren().isEmpty()) {
            level++;
            int finalLevel = level;
            node.getChildren().values().stream().forEach((x) -> {
                printBranch(x.get(), finalLevel);
            });
        }
    }

    public static void renew(FileTree fileTree) {
        Consumer<TreeNode<Path>> consumer = new Consumer<TreeNode<Path>>() {
            @Override
            public void accept(TreeNode<Path> node) {
                if (!Files.exists(node.getPath()))
                    node.getParent().removeChild(node.getKey());
            }
        };
        fileTree.setRoot(consumerOnNodeTree(fileTree.getRoot(), consumer));
    }

    private static TreeNode<Path> consumerOnNodeTree(TreeNode<Path> rootNode, Consumer<TreeNode<Path>> consumer) {
        consumer.accept(rootNode);
        if (!rootNode.getChildren().isEmpty()) {
            rootNode.getChildren().forEach((k, v) -> consumerOnNodeTree(v.get(), consumer));
        }
        return rootNode;
    }

    private static boolean predicateOnNodeTree(TreeNode<Path> rootNode, Predicate<TreeNode<Path>> predicate) {
        AtomicBoolean result = new AtomicBoolean(false);
        result.set(predicate.test(rootNode));
        if (result.get()) return result.get();
        if (!rootNode.getChildren().isEmpty()) {
            result.set(rootNode.getChildren().values().stream().anyMatch((v) -> (predicateOnNodeTree(v.get(), predicate))));
        }
        return result.get();
    }

    private static TreeNode<Path> unaryOnNodeTree(TreeNode<Path> rootNode, UnaryOperator<TreeNode<Path>> unary) {
        TreeNode<Path> result = null;
        result = unary.apply(rootNode);
        if (result != null) return result;
        if (!rootNode.getChildren().isEmpty()) {
            result = rootNode.getChildren().values().stream()
                    .map((v) -> (unaryOnNodeTree(v.get(), unary))).filter((x) -> (x != null)).findFirst().orElse(null);
            return result;
        }
        return result;
    }

    //defines address count difference. 0 = same, + = a>b, - = a<b. digit = difference level count
    private static int diff(Path a, Path b) {
        return a.getNameCount() - b.getNameCount();
    }

    private static TreeNode<Path> shorter(TreeNode<Path> a, TreeNode<Path> b) {
        if (diff(a.getPath(), b.getPath()) > 0)
            return b;
        else if (diff(a.getPath(), b.getPath()) < 0) return a;
        else return null;
    }

    private static TreeNode<Path> longer(TreeNode<Path> a, TreeNode<Path> b) {
        if (diff(a.getPath(), b.getPath()) > 0)
            return a;
        else if (diff(a.getPath(), b.getPath()) < 0) return b;
        else return null;
    }

    //test of the same file system root of FileTrees
    static boolean isSameRootPoint(FileTree a, FileTree b) {
        return isSameRoot(a.getRootPoint(), b.getRootPoint());
    }

    private static boolean isSameRoot(Path a, Path b) {
        try {
            return Files.isSameFile(a, b);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    //returns new root path, children on the ways to a and b or null if same file or begining of
    //one path equals to another path (ex: c:/1/2/3/4 and c:/1/2 - returns null;
    // c:/1/2/3 and c:/1/3/5 - returns c:/1, c:/1/2/, c:/1/3)
    private static Path[] pathOverlap(Path a, Path b) {
        try {
            if (!Files.isSameFile(a.getRoot(), b.getRoot()))
                return null;
            else {
                int diff = diff(a, b);
                Path[] result = Stream.iterate(0, x -> x + 1).limit(Math.min(a.getNameCount(), b.getNameCount()) - 1)
                        .map((x) -> {
                            try {
                                if (!Files.isSameFile(a.getName(x), b.getName(x))) {
                                    Path[] arr = {a.getParent(), a.getName(x), b.getName(x)};
                                    return arr;
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            return null;
                        }).findFirst().orElse(null);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    //build nodes between start and end, returns start node;
    private static TreeNode<Path> buildNodes(Path start, TreeNode<Path> end) {
        try {
            if (Files.isSameFile(start, end.getPath())) {
                return end;
            }
            if (!(Files.find(start, Integer.MAX_VALUE, (p, a) -> {
                try {
                    Files.isSameFile(p, end.getPath());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return false;
            }).findFirst().orElse(null) == null))
                return null;
            else {
                Path child = end.getPath();
                Path parent;
                TreeNode<Path> childN = end;
                TreeNode<Path> parentN;
                do {
                    parent = child.getParent();
                    parentN = new TreeNode<>(parent.getFileName().toString(), parent);
                    parentN.addChild(childN.getKey(), childN);
                    child = parent;
                    childN = parentN;
                } while (parent != start);
                return parentN;

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    //finds path of longerRootNode, that is not contained in the nodes under shortRooNode
    //returns first not contained in the nodes path
    private static Path overlapInTree(TreeNode<Path> shorterRootNode, TreeNode<Path> longerRootNode) {
        try {
            if (!longerRootNode.getPath().toAbsolutePath().startsWith(shorterRootNode.getPath().toAbsolutePath())
                    || Files.isSameFile(longerRootNode.getPath(), shorterRootNode.getPath()))
                return null;
            else {
                Path relative = shorterRootNode.getPath().relativize(longerRootNode.getPath());
                int diff = diff(longerRootNode.getPath(), shorterRootNode.getPath());
                TreeNode<Path> node = shorterRootNode;
                for (int i = 0; i < relative.getNameCount(); i++) {
                    if (node.contains(relative.getName(i).getFileName().toString())) {
                        node = node.getChild(relative.getName(i).getFileName().toString());
                        continue;
                    } else {
                        return relative.getName(i);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    //unites children of two TreeNodes<Path> with same root path. checked status migrates from A to children of B
    private static TreeNode<Path> unite(TreeNode<Path> rootNodeA, TreeNode<Path> rootNodeB) {
        try {
            if (Files.isSameFile(rootNodeA.getPath().getRoot(), rootNodeB.getPath().getRoot())) {
                rootNodeB.getChildren().values().parallelStream().forEach((x) -> {
                    rootNodeA.getChildren().putIfAbsent(x.get().getKey(), x);
                    x.get().setParent(rootNodeA);
                    x.get().setChecked(rootNodeA.isChecked());
                });
                return rootNodeA;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static TreeNode<Path> addBranch(FileTree actual, FileTree newBranch) {
        return addBranch(actual, newBranch.getRoot());
    }

    public static TreeNode<Path> addBranch(FileTree actual, TreeNode<Path> newBranch) throws IllegalArgumentException {
        Path rootPath = actual.getRoot().getPath();
        Path newBrRootPath = newBranch.getPath();
        if (isSameRoot(actual.getRootPoint(), newBranch.getPath())) {
            try {
                if (Files.isSameFile(actual.getRoot().getPath(), newBranch.getPath())) {
                    actual.setRoot(unite(actual.getRoot(), newBranch));
                    return actual.getRoot();
                }
                int diff = diff(rootPath, newBrRootPath);

                Path[] overlap = pathOverlap(rootPath, newBrRootPath);
                if (overlap != null) {
                    TreeNode<Path> newRoot = buildNodes(overlap[0], actual.getRoot());
                    TreeNode<Path> newRoot2 = buildNodes(overlap[0], newBranch);
                    newRoot = unite(newRoot, newRoot2);
                    actual.setRoot(newRoot);
                    return actual.getRoot();
                } else {
                    TreeNode<Path> shorter = shorter(actual.getRoot(), newBranch);
                    TreeNode<Path> longer = longer(actual.getRoot(), newBranch);
                    if (shorter == null)
                        return null;
                    Path path = overlapInTree(shorter, longer);
                    TreeNode<Path> node = buildNodes(path, longer);
                    TreeNode<Path> rNode = find(shorter, path);
                    rNode.addChild(node.getKey(), node);
                    actual.setRoot(shorter);
                    return actual.getRoot();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
